from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

#from selenium.webdriver.common.
from constants import WAIT_TIMEOUT
import time

class CreateProgramFeature:

    def __init__(self, driver):
        self.driver = driver

    def createProgram(self, programNumber):
        #chain
        self.programNumber = str(programNumber)

        self.__openBurger()
        self.__openAdminConsole()
        self.__openProgram()
        self.__clickNewProgram()
        self.__fillProgramData()
        self.__selectUsers()
        self.__selectMilestones()
        self.__sendInvitations()
        pass

    def __openBurger(self):
        WebDriverWait(self.driver, WAIT_TIMEOUT).until(
            EC.presence_of_all_elements_located((By.CLASS_NAME, "hamBurger"))
        )    
        time.sleep(1)

        hamBurgerIconButton = self.driver.find_element_by_class_name("hamBurger")
        hamBurgerIconButton.click()

    def __openAdminConsole(self):
        time.sleep(1)

        self.driver.find_element_by_link_text("Admin Console").click()


    #ui-sref="app.program"
    def __openProgram(self):
        time.sleep(5) #this should be excluded later
        link = self.driver.find_element_by_xpath("//md-card[@ui-sref='app.program']")
        link.click()
        print(link)

    def __clickNewProgram(self):
        WebDriverWait(self.driver, WAIT_TIMEOUT).until(
            EC.presence_of_all_elements_located((By.XPATH, "//button[contains(.,'New Program')]"))
        )    
        time.sleep(2)
        newProgramButton = self.driver.find_element_by_xpath("//button[contains(.,'New Program')]")
        newProgramButton.click()
        
    def __fillProgramData(self):
        ##print('wainting to load sub and subcategories')
        WebDriverWait(self.driver, WAIT_TIMEOUT).until(
            EC.presence_of_all_elements_located((By.XPATH, "//div[@id='cat']"))
        )    

        WebDriverWait(self.driver, WAIT_TIMEOUT).until(
            EC.presence_of_all_elements_located((By.XPATH, "//input[@name='name']"))
        )    
        self.driver.implicitly_wait(5)

        nameInput = self.driver.find_element_by_xpath("//input[@name='name']")
        nameInput.send_keys('autoprog'+self.programNumber)

        shortnameInput = self.driver.find_element_by_xpath("//input[@name='shortname']")
        shortnameInput.send_keys('autopr'+self.programNumber)

        descriptionInput = self.driver.find_element_by_xpath("//textarea[@name='decsription']")
        descriptionInput.send_keys('automatically generated program. Nr: '+self.programNumber)
        
        selectAll = self.driver.find_element_by_xpath("//div[@id='cat']//button[contains(.,'Select All')]")
        selectAll.click()

        selectAll = self.driver.find_element_by_xpath("//div[@id='subCat']//button[contains(.,'Select All')]")
        selectAll.click()

        submit = self.driver.find_element_by_xpath('//button[@type="submit"]')
        submit.click()

    def __selectUsers(self):
        self.driver.implicitly_wait(5)
        
        roleSelect = self.driver.find_element_by_xpath("//select")
        roleSelect.click()

        optElement = self.driver.find_element_by_xpath("//select//option[2]")
        optElement.click()

        inputUsers = self.driver.find_element_by_xpath("//input[@type='search']")

        for userI in range(3):
            inputUsers.click()
            self.driver.implicitly_wait(1)
        
            user = self.driver.find_element_by_xpath("//ul[contains(@class, 'ui-select-choices')]//div[3]//span//div")
            user.click()

        self.driver.implicitly_wait(1)
        
        submit = self.driver.find_element_by_xpath("(//button[contains(text(), 'Save & Continue')])[2]")
        submit.click()

    def __selectMilestones(self):
        self.driver.implicitly_wait(3)
        #saveAndContinue = self.driver.find_element_by_xpath("(//button//span[contains(text(), 'Save & Continue')])[1]")

        WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//add-milestone-template-common-template//div//div[3]//button")))
        
        saveAndContinue = self.driver.find_element_by_xpath("//add-milestone-template-common-template//div//div[3]//button")
        saveAndContinue.click()
            
    def __sendInvitations(self):
        #all_usersinvite_datatable_wrapper

        #//th//input[contains(@class, 'leftMargin ng-pristine ng-valid ng-touched')]
        #//div[@id='all_usersinvite_datatable_wrapper']
        #WebDriverWait(self.driver, 20).until(
        #    EC.presence_of_all_elements_located((By.ID, "all_usersinvite_datatable_wrapper"))
        #)

        WebDriverWait(self.driver, 20).until(
            #EC.presence_of_all_elements_located((By.XPATH, "//div[@id='all_usersinvite_datatable_wrapper']//table//thead//tr//th//input[contains(@class, 'leftMargin ng-pristine ng-valid ng-touched')]"))
            EC.presence_of_all_elements_located((By.XPATH, "//div[@id='all_usersinvite_datatable_wrapper']//table//thead//tr//th//input"))
        )

        self.driver.implicitly_wait(3)

        print("waited for all")

        #//div[@id='all_usersinvite_datatable_wrapper']//th/
        #checkAll = self.driver.find_element_by_xpath("//div[@id='all_usersinvite_datatable_wrapper']//table//thead//tr//th//input[contains(@class, 'leftMargin ng-pristine ng-valid ng-touched')]")
        
        checkAll = self.driver.find_element_by_xpath("//div[@id='all_usersinvite_datatable_wrapper']//table//thead//tr//th//input")
        
        checkAll.click()

        #///Send Invitations
        self.driver.implicitly_wait(1)
        sendInvitations = self.driver.find_element_by_xpath("//button[contains(text(), 'Send Invitations')]")
        sendInvitations.click()

        WebDriverWait(self.driver, 20).until(
            EC.presence_of_all_elements_located((By.XPATH, "//button[contains(text(), 'OK')]"))
        )

        okBtn = self.driver.find_element_by_xpath("//button[contains(text(), 'OK')]")
        okBtn.click()
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from constants import WAIT_TIMEOUT
import time
class LoginFeature:

    def __init__(self, driver, loginUrl):
        self.driver = driver
        self.loginUrl = loginUrl
        
        
    def login(self, username, password):
        self.driver.get(self.loginUrl)
        
        #time.sleep(15)
        awaiter = WebDriverWait(self.driver, WAIT_TIMEOUT).until(
            EC.presence_of_all_elements_located((By.NAME, "email"))
        )    

        time.sleep(1)
        emailInput = self.driver.find_element_by_name("email")
        emailInput.send_keys(username)
        #emailInput.send_keys(Keys.RETURN)

        passwordInput = self.driver.find_element_by_name("password")
        passwordInput.send_keys(password)
        #passwordInput.send_keys(Keys.RETURN)

        submit = self.driver.find_element_by_xpath('//button[@type="submit"]')

        submit.click()